import ruamel.yaml
import os
from os import path, sep


class Yaml:
    __yaml = ruamel.yaml.YAML()
    __file = ""
    __path = ""
    __yamlFile = ""

    __yamlFileRead = ""
    __yamlFileWritte = ""

    def readYaml(self, file: path):
        self.__path = file
        with open(self.__path,'r') as self.__file:
            self.__yamlFile = self.__yaml.load(self.__file)
        return self.__yamlFile

    def writteYaml(self, src: path, dest: path):
        self.__yamlFileRead = self.readYaml(src)
        self.__yamlFileWritte = self.readYaml(dest)
        self.__yamlFileWritte = self.__yamlFileRead
        with open(dest, 'w') as YFW:
            self.__yaml.dump(self.__yamlFileWritte,YFW)

    def overideAlacrittyColors(self, src: path, fileAlacritty: path):
        self.__yamlFileRead = self.readYaml(src)
        self.__yamlFileWritte = self.readYaml(fileAlacritty)
        self.__yamlFileWritte['colors'] = self.__yamlFileRead['colors']
        with open(fileAlacritty, 'w') as YFW:
            self.__yaml.dump(self.__yamlFileWritte,YFW)