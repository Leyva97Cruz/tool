import ruamel.yaml

theme = "themes/hyper.yaml"  # Files to select themes
fileAlacritty = "config/example.yml"  # File to change colors
yaml = ruamel.yaml.YAML()

if __name__ == "__main__":
    print(theme)
    updateFileAlacritty = open(fileAlacritty, 'r')
    fileLines = open(theme, 'r')

    readAlacrittyFile = yaml.load(updateFileAlacritty)
    readFileLines = yaml.load(fileLines)

    readAlacrittyFile['colors'] = readFileLines['colors']
    updateFileAlacritty.close()
    fileLines.close()

    updateFile = open(fileAlacritty, 'w')
    yaml.dump(readAlacrittyFile, updateFile)
    updateFile.close()
