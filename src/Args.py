import argparse

class Argument:
    __parser = ""

    def cli(self):
        self.__parser = argparse.ArgumentParser(
            prog='Pycolor',
            description='Comand line interface for change theme in alacritty'
        )
        self.__parser.add_argument(
            '-t',
            '--theme',
            help='name of the theme'
        )
        self.__parser.add_argument(
            '-f',
            '--font',
            help='name of the font'
        )
        return self.__parser.parse_args()
