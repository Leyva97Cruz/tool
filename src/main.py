import Path
import YamIO
import Args


def main():
    ArgsClass = Args.Argument()
    YamlClass = YamIO.Yaml()
    PathClass = Path.PyPath()
    cli = ArgsClass.cli()
    theme = cli.theme
    path = PathClass.getPathThemeYaml(theme + '.yaml')
    path2 = PathClass.setPathDirFile('config', 'example.yml')
    writteAlacritty = YamlClass.overideAlacrittyColors(path, path2)


if __name__ == "__main__":
    main()