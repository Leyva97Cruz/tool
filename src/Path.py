import os
from os import path


class PyPath:
    __Path = ""
    __PathFile = ""

    def getPathThemeYaml(self, file):
        self.__Path = path.join(
            os.getcwd(),
            'themes')
        self.__PathFile = path.join(
            self.__Path,
            file
        )
        return self.__PathFile

    def setPath(self, file):
        self.__Path = path.join(
            os.getcwd(),
            file
        )
        return self.__Path

    def setPathDirFile(self, src: path, file: path):
        self.__Path = path.join(
            os.getcwd(),
            src)
        self.__PathFile = path.join(
            self.__Path,
            file
        )
        return self.__PathFile
